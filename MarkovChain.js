var fs = require('fs');

function MarkovChain(options) {
	this.order = options.order || 1;
	this.filename = options.filename || 'default-' + this.order;
	this.data = {};
}

MarkovChain.prototype.learn = function (states) {
	var depth = 0;
	for(var i = 0, l = states.length + 1; i < l; i++) {
		if(typeof states[i] == 'undefined') {
			states[i] = '';
		}
		var stateContent = states[i];
		var predecessors = [];
		for(var j = this.order; j > 0; j--) {
			if(typeof states[i-j] == 'undefined') {
				states[i-j] = '';
			}
			predecessors.push(states[i-j]);
		}
		console.log(predecessors);
		predecessors = this._padStates(predecessors);
		this._addInstance(stateContent, predecessors);
	}
}

MarkovChain.prototype._addInstance = function (stateContent, predecessors) {
	var stateLocation = this.data;
	for(var i = predecessors.length - this.order; i < predecessors.length - 1; i++) {
		if(typeof stateLocation[predecessors[i]] == 'undefined') {
			stateLocation[predecessors[i]] = {};
		}
		stateLocation = stateLocation[predecessors[i]];
	}
	
	if(typeof stateLocation[predecessors[predecessors.length - 1]] == 'undefined') {
		stateLocation[predecessors[predecessors.length - 1]] = {
			successorInstanceCount: 0,
			successors: []
		};
	}
	
	stateLocation = stateLocation[predecessors[predecessors.length - 1]];
	var state;
	
	for(var i = 0, l = stateLocation.successors.length; i < l; i++) {
		if(stateLocation.successors[i].stateContent == stateContent) {
			state = stateLocation.successors[i];
		}
	}
	
	stateLocation.successorInstanceCount++;
	
	if(typeof state == 'undefined') {
		stateLocation.successors.push({
			stateContent: stateContent,
			instanceCount: 1
		});
	} else {
		state.instanceCount++;
	}
}

MarkovChain.prototype._padStates = function (states) {
	while(states.length < this.order) {
		states.unshift('');
	}
	
	return states;
}

MarkovChain.prototype.saveFile = function () {
	var that = this;
	fs.writeFile(this.filename + '.json', JSON.stringify({order: this.order, data: this.data}), function (err) {
		if(err) {
			console.log(err);
		} else {
			console.log(that.filename + '.json saved');
		}
	});
}

MarkovChain.prototype.loadFile = function (callback) {
	var that = this;
	fs.readFile(this.filename + '.json', function (err, data) {
		if(err) {
			console.log(err);
		} else {
			if(data.length == 0) {
				console.log(that.filename + '.json empty, nothing loaded');
				callback();
				return;
			}
			var fileContent = JSON.parse(data);
			that.order = fileContent.order;
			that.data = fileContent.data;
			console.log(that.filename + '.json loaded');
		}
		callback();
	});
}

MarkovChain.prototype.getNextState = function (states) {
	var statesCopy = states.slice(0);
	statesCopy = this._padStates(statesCopy);
	var stateLocation = this.data;
	
	for(var i = statesCopy.length - this.order; i < statesCopy.length; i++) {
		stateLocation = stateLocation[statesCopy[i]];
	}
	
	if(typeof stateLocation == 'undefined') {
		return '';
	} else {
		var progress = 0;
		var target = Math.random() * stateLocation.successorInstanceCount;
		var nextStateContent = '';
		for(var i = 0; i < stateLocation.successors.length; i++) {
			if(progress + stateLocation.successors[i].instanceCount >= target) {
				nextStateContent = stateLocation.successors[i].stateContent;
				break;
			} else {
				progress += stateLocation.successors[i].instanceCount;
			}
		}
		return nextStateContent;
	}
}

module.exports = MarkovChain;