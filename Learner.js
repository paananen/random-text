var MarkovChain = require('./MarkovChain');

function Learner(options, callback) {
	this.type = options.type || Learner.WORDS_LEARNER;
	this.chain = new MarkovChain(options);
	this.chain.loadFile(callback);
}

Learner.WORDS_LEARNER = 1;
Learner.LETTERS_LEARNER = 2;

Learner.prototype.learn = function (text) {
	if(this.type == Learner.LETTERS_LEARNER) {
		this._learnLetters(text);
	} else if(this.type == Learner.WORDS_LEARNER) {
		this._learnWords(text);
	}
}

Learner.prototype._learnLetters = function (text) {
	var letters = Array.from(text);
	this.chain.learn(letters);
}

Learner.prototype._learnWords = function (text) {
	// var words = text.match(/([a-zA-ZåäöÅÄÖ:-]+|[.,?!]+|[0-9,.]+)/g);
	var words = text.match(/([a-zA-ZåäöÅÄÖ0-9:.,?!#@€$%&-]+)/g);
	if(words != null) {
		this.chain.learn(words);
	}
}

module.exports = Learner;