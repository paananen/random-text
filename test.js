var Learner = require('./Learner');
var Generator = require('./Generator');
var pseudoFinnishGenerator;
var pseudoFinnishLearner = new Learner({
	order: 2,
	filename: 'finnish-words-2'
}, function () {
	pseudoFinnishGenerator = new Generator({chain: pseudoFinnishLearner.chain});
	// pseudoFinnishLearner.learn("Pöytätenniksen Suomen-mestaruudesta on kilpailtu vuodesta 1938 lähtien.");
	// pseudoFinnishLearner.learn("Tuolloin pelattiin neljässä eri sarjassa: miesten kaksinpeli, miesten nelinpeli, naisten kaksinpeli ja sekanelinpeli.");
	// pseudoFinnishLearner.learn("Vuonna 1948 ohjelmaan tuli lisäksi naisten nelinpeli.");
	// pseudoFinnishLearner.learn("Brawn GP oli talli, joka kilpaili Formula 1 -sarjassa kaudella 2009. Brawn GP nousi F1-sarjaan lopettaneen Honda-tallin paikalle.");
	// pseudoFinnishLearner.learn("Talli nimettiin aiemmin Hondan tallipäällikkönä toimineen Ross Brawnin mukaan, josta tuli sekä uuden tallin päällikkö että omistaja maksettuaan Hondalle symbolisen yhden punnan hinnan siitä.");
	// pseudoFinnishLearner.learn("Tallin kuljettajina toimivat koko kauden ajan entiset Honda-kuljettajat Jenson Button ja Rubens Barrichello.");
	// pseudoFinnishLearner.learn("Brawn voitti kauden seitsemästä ensimmäisestä kilpailusta kuusi; vain Kiinan GP:ssä Red Bull -talli oli parempi ja sen kuljettajat juhlivat kaksoisvoittoa Buttonin jäätyä kolmanneksi.");
	// pseudoFinnishLearner.learn("Seuraavissa kilpailuissa auton nopeus ei enää riittänyt voittoihin: Turkissa tuli tallin historian ensimmäinen keskeytys, kun Rubens Barrichellon vaihdelaatikko hajosi, Britannian GP:ssä Button jäi ensimmäistä kertaa kauden aikana palkintopallin ulkopuolelle sijoituttuaan kuudenneksi ja Saksan GP:ssä Brawnin auto jäi ensimmäistä kertaa kauden aikana palkintojen ulkopuolelle.");
	// pseudoFinnishLearner.learn("Kinkku ananas kebab jauheliha tomaatti");
	// pseudoFinnishLearner.learn("Kinkku kebab ananas jauheliha tomaatti");
	// pseudoFinnishLearner.learn("Kinkku jauheliha tomaatti ananas kebab");
	console.log(pseudoFinnishGenerator.generateString());
});