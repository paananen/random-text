var MarkovChain = require('./MarkovChain');

function Generator(options) {
	this.type = options.type || Generator.WORDS_GENERATOR;
	this.chain = options.chain || new MarkovChain();
}

Generator.WORDS_GENERATOR = 1;
Generator.LETTERS_GENERATOR = 2;

Generator.prototype.generateString = function () {
	var states = [];
	while(states.length < 1 || states[states.length - 1] != '') {
		states.push(this.chain.getNextState(states));
	}
	
	if(this.type == Generator.WORDS_GENERATOR) {
		for(var i = 0; i < states.length; i++) {
			var stateSucceededWithSpace = !(/(^[.,?!]+)/g.test(states[i + 1]));
			if(stateSucceededWithSpace) {
				states.splice(i + 1, 0, ' ');
				i++;
			}
		}
	}
	
	var output = states.join('').trim();
	return output;
}

module.exports = Generator;